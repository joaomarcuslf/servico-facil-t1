<?php
include('./src/actions/redirectIfAuthenticated.php');

$pageTitle = 'Página de Login';
?>

<!DOCTYPE html>
<html lang="en">
  <?php include("./src/components/header.php") ?>

  <body>
    <?php include("./src/components/navbar.php") ?>

    <section class="hero is-primary">
      <div class="hero-body">
        <div class="container is-fluid">
          <h1 class="title">
            Login
          </h1>
        </div>
      </div>
    </section>

    <?php include("./src/components/formError.php") ?>

    <form class="hero" method="post" action="./src/actions/login.php">
      <div class="hero-body">
        <div class="container">
          <div class="columns">
            <div class="column is-8 is-offset-2">
              <div class="login-form">
                <p class="control has-icon has-icon-right">
                  <label class="label">E-mail:</label>
                  <input class="input email-input" placeholder="nome@dominio.com" type="text" name="userEmail" required />
                  <span class="icon user">
                    <i class="fa fa-user"></i>
                  </span>
                </p>

                <p class="control has-icon has-icon-right">
                  <label class="label">Senha:</label>
                  <input class="input password-input" type="password" name="userPassword" required placeholder="Digite sua senha" />
                  <span class="icon user">
                    <i class="fa fa-lock"></i>
                  </span>
                </p>

                <hr />

                <p class="control login">
                  <button class="button is-primary is-outlined is-large is-fullwidth" button="submit">Entrar</button>
                </p>
              </div>

              <div class="section">
                <p class="has-text-centered">
                  É prestador de serviço ou precisa de alguém e não está cadastrado?<br />
                  <a href="./register.php">Não perca mais tempo!</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </body>
</html>