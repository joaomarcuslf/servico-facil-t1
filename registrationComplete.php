<?php
include('./src/actions/redirectIfAuthenticated.php');

$pageTitle = 'Obrigado por se cadastar';
?>

<!DOCTYPE html>
<html lang="en">
  <?php include("./src/components/header.php") ?>

  <body>
    <section class="hero is-primary is-fullheight has-text-centered">
      <?php include("./src/components/navbar.php") ?>

      <div class="hero-body">
        <div class="container is-fluid">
          <h1 class="title">
            Parabéns!!
          </h1>
          <h2 class="subtitle">
            Você foi cadastrado com sucesso, em breve deverá chegar um e-mail para a confirmação do seu cadastro
          </h2>
        </div>
      </div>
    </section>
  </body>
</html>