<?php if($_GET["error"]) : ?>
  <section class="hero is-danger is-bold">
    <div class="hero-body">
      <div class="container">
        <h1 class="title">
          Foram encontrados erros no seu formulário!
        </h1>
        <h2 class="subtitle">
          Por favor, verifique seus dados e tente novamente
        </h2>
      </div>
    </div>
  </section>
<?php endif; ?>