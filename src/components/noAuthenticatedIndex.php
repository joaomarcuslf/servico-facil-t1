<section class="hero is-primary is-fullheight has-text-centered">
  <?php include("./src/components/navbar.php") ?>

  <div class="hero-body">
    <div class="container is-fluid">
      <h1 class="title">
        Bem-Vindo ao Serviço Fácil
      </h1>
      <h2 class="subtitle">
        Se você é prestador de serviços, ou é alguém que precisa de um serviço específico, aqui você entcontra!<br />
        Não perca mais tempo!
        <hr />
        <a class="button is-white is-outlined is-large" href="register.php">Cadastre-se agora!</a>
      </h2>
    </div>
  </div>
</section>